var express = require('express');
var app = express();
var aws = require('aws-sdk');

aws.config.update({ region: 'us-east-1' });
var SNS = new aws.SNS();

var params = {
    TopicArn: 'SNS_ARN_GOES_HERE'
}


app.get('/publishCreate', function (req, res) {

    params.Message = 'Testing Create';
    params.MessageAttributes = {
        "key": {
            DataType: "String",
            StringValue: "key1"
        },
        "sort": {
            DataType: "String",
            StringValue: "sort1"
        },
        "command": {
            DataType: "String",
            StringValue: "Create"
        },
        "useridfrom": {
            DataType: "String",
            StringValue: "Me"
        },
        "useridto": {
            DataType: "String",
            StringValue: "You"
        },
        "body": {
            DataType: "String",
            StringValue: "Hello!"
        }
    };

    SNS.publish(params, (err, data) => {
        if (err) {
            console.log("Error publishing message CREATE to SNS", err);
            res.send(err);
        } else {
            console.log("Success publishing message CREATE to SNS", data.MessageId);
            res.send(data);
        }
    })
});

app.get('/publishRead', function (req, res) {

    params.Message = 'Testing Read';
    params.MessageAttributes = {
        "key": {
            DataType: "String",
            StringValue: "key1"
        },
        "sort": {
            DataType: "String",
            StringValue: "sort1"
        },
        "command": {
            DataType: "String",
            StringValue: "Read"
        }
    };

    SNS.publish(params, (err, data) => {
        if (err) {
            console.log("Error publishing message READ to SNS", err);
            res.send(err);
        } else {
            console.log("Success publishing message READ to SNS", data.MessageId);
            res.send(data);
        }
    });
});

app.get('/publishUpdate', function (req, res) {

    params.Message = 'Testing Update';
    params.MessageAttributes = {
        "key": {
            DataType: "String",
            StringValue: "key1"
        },
        "sort": {
            DataType: "String",
            StringValue: "sort1"
        },
        "command": {
            DataType: "String",
            StringValue: "Update"
        },
        "body": {
            DataType: "String",
            StringValue: "Goodbye!"
        }
    };

    SNS.publish(params, (err, data) => {
        if (err) {
            console.log("Error publishing message UPDATE to SNS", err);
            res.send(err);
        } else {
            console.log("Success publishing message UPDATE to SNS", data.MessageId);
            res.send(data);
        }
    });
});

app.get('/publishDelete', function (req, res) {

    params.Message = 'Testing Delete';
    params.MessageAttributes = {
        "key": {
            DataType: "String",
            StringValue: "key1"
        },
        "sort": {
            DataType: "String",
            StringValue: "sort1"
        },
        "command": {
            DataType: "String",
            StringValue: "Delete"
        },			
        "body": {
            DataType: "String",
            StringValue: "Hello!"
        }
    };

    SNS.publish(params, (err, data) => {
        if (err) {
            console.log("Error publishing message DELETE to SNS", err);
            res.send(err);
        } else {
            console.log("Success publishing message DELETE to SNS", data.MessageId);
            res.send(data);
        }
    });
});

var server = app.listen(8080, '0.0.0.0', function () {
    var host = server.address().address;
    var port = server.address().port;

    console.log('Listening at http://%s:%s', host, port);
});
