# SNS_test

## Instrucciones para integrar con [PostProcessorLambda](https://bitbucket.org/u201410964/postprocessorlambda)
* Crear un nuevo topic en el dashboard de SNS.
* Cambiar el trigger CloudWatch Events por el trigger SNS 
* Elegir el topic creado dentro de la lista.
* Marcar Enabled.

## Instrucciones para usar este proyecto
* Pegar el Topic ARN que aparecerá en la página de detalles del topic creado.
* Dentro de SQS, dar click derecho a la cola y elegir Subscribe Queue to SNS Topic y elegir el topic creado.
